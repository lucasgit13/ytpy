#!/bin/sh
[ -z "$VIRTUAL_ENV" ] && echo "VIRTUAL_ENV not set" && exit 1
PYTHON_EXECUTABLE="$VIRTUAL_ENV/bin/python"
clipboard=$(xclip -selection clipboard -o)
echo "[CLIPBOARD]: $clipboard"
opts=$(printf '%b' "clipboard\nplay-from-history\nedit-history\nget-description" | dmenu -i || exit 1)

play_fn() {
    test -z "$1" && exit 1
    $PYTHON_EXECUTABLE ytpy.py "$1"
}

case "$opts" in
    "play-from-history")
        video=$(sqlite3 history.db -column "select * from videos;" | tac | dmenu -i -l 20)
        test -z "$video" && exit 1
        video_url="https://www.youtube.com/watch?v=$(printf '%s' "$video" | cut -d ' ' -f1)"
        play_fn "$video_url"
        ;;
    "edit-history")
        video=$(sqlite3 history.db -column "select * from videos;" | tac | dmenu -i -l 20 -p 'delete:')
        test -z "$video" && exit 1
        video_id=$(printf '%s' "$video" | cut -d ' ' -f1)
        sqlite3 history.db "delete from videos where video_id = '$video_id'" &&
        echo "[DELETED]: $video"
        exit 0
        ;;
    "clipboard")
        play_fn "$clipboard"
        ;;
    "get-description")
        video=$(sqlite3 history.db -column "select * from videos;" | tac | dmenu -i -l 20)
        test -z "$video" && exit 1
        video_url="https://www.youtube.com/watch?v=$(printf '%s' "$video" | cut -d ' ' -f1)"
        printf '%b' "quering description...\n"
        yt-dlp --get-description "$video_url"
        ;;
    *)
        exit 1
        ;;
    esac
