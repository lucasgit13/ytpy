from collections.abc import Iterable
from subprocess import Popen, PIPE
from typing import Optional


class Dmenu:
    def __init__(
        self,
        center: Optional[bool] = False,
        index_mode: Optional[bool] = False,
        insensitive: Optional[bool] = False,
        lines: int = 0,
        prompt: Optional[str] = "",
    ) -> None:
        self.center = center
        self.index_mode = index_mode
        self.insensitive = insensitive
        self.lines = lines
        self.prompt = prompt
        self.flags_str = []

        if self.index_mode:
            self.flags_str.append("-ix")
        if self.insensitive:
            self.flags_str.append("-i")
        if self.center:
            self.flags_str.append("-c")
        if self.lines:
            self.flags_str.append("-l")
            self.flags_str.append(str(self.lines))
        if self.prompt:
            self.flags_str.append("-p")
            self.flags_str.append(str(self.prompt))

    def run(self, items: list[str]) -> str:
        cmd = ["dmenu"] + self.flags_str
        dmenu_in = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE)

        if not dmenu_in.stdin:
            return ""

        for item in items:
            if item:
                w = bytes(item + "\n", "utf-8")
                dmenu_in.stdin.write(w)

        dmenu_out = dmenu_in.communicate()[0]
        output = dmenu_out.decode("utf-8")
        if output:
            return output.strip()
        else:
            return ""

    def run_get_index(self, items: list[Iterable[str]], separator: str) -> int | None:
        cmd = ["dmenu"] + self.flags_str
        dmenu_in = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        self.index_mode = True

        if not dmenu_in.stdin:
            return None

        for item in items:
            s = f"{separator}".join(item)
            w = bytes(s + "\n", "utf-8")
            dmenu_in.stdin.write(w)

        dmenu_out = dmenu_in.communicate()[0]
        output = dmenu_out.decode("utf-8")
        if output:
            index = int(output)
            return index
        else:
            return None

