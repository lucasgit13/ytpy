import sqlite3
import logging
from os import getenv

DB_FILE = getenv('YTPY_DB_FILE') or "/home/lucas/.scripts/settings/history.db"


def get_full_class_name(obj):
    module = obj.__class__.__module__
    if module is None or module == str.__class__.__module__:
        return obj.__class__.__name__
    return module + "." + obj.__class__.__name__


def insert_record(cursor, table, video_id, title):
    cursor.execute(f"INSERT INTO {table} VALUES ('{video_id}', '{title}')")


def delete_record(cursor, table, video_id):
    cursor.execute(f"DELETE FROM {table} WHERE video_id = '{video_id}'")


connection = sqlite3.connect(DB_FILE)
cursor = connection.cursor()

command_create_table = """CREATE TABLE IF NOT EXISTS
videos(video_id TEXT PRIMARY KEY, title TEXT)
"""

cursor.execute(command_create_table)

table = "videos"


def add(video_id, title):
    try:
        insert_record(cursor, table, video_id, title)
    except sqlite3.IntegrityError as _:
        delete_record(cursor, table, video_id)
        insert_record(cursor, table, video_id, title)
    except Exception as another:
        print(f"Exception: {get_full_class_name(another)}")
        raise another

    connection.commit()
    logging.info("history updated successfully!")


def fetch_history() -> list[str]:
    videos = []
    try:
        cur = cursor.execute("SELECT * FROM videos")
        videos = cur.fetchall()
    except Exception as e:
        print(e)
        return videos
    finally:
        videos.reverse()
        return ["  ".join(video) for video in videos]


def delete_from_history(video_id: str):
    delete_record(cursor, table, video_id)
    connection.commit()
