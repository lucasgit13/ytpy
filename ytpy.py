from py_youtube.pyyoutube import re
from dmenu import Dmenu
from py_youtube import Data
from re import split
from subprocess import Popen, PIPE, run
from shutil import which
import history
import logging
import pyperclip
import sys
import threading

level = logging.DEBUG
fmt = "[%(levelname)s] - %(message)s"
logging.basicConfig(level=level, format=fmt)

dmenu_opts = Dmenu(insensitive=True)
dmenu_his = Dmenu(insensitive=True, lines=20)
dmenu_desc = Dmenu(insensitive=True, prompt="url from:")
main_opts = [
    "clipboard",
    "play-from-history",
    "edit-from-history",
    "get-description",
]

url = ""
clipboard = pyperclip.paste()


def play(url):
    if not url:
        logging.error("no url exiting...")
        sys.exit(1)

    logging.info("running mpv...\n")
    result = Popen(["mpv", url], stdout=PIPE, stderr=PIPE)
    result.communicate()


def get_video_description(*args):
    result = run(args, capture_output=True, text=True)
    stdout = result.stdout
    stderr = result.stderr
    if stdout:
        logging.info(f"VIDEO DESCRIPTION:\n{stdout}\n")
    if stderr:
        logging.info(f"VIDEO DESCRIPTION:\n{stderr}\n")


def query_video_from_history(
    dmenu_profile: Dmenu, video_id: str
) -> tuple[str, str, str]:
    video = dmenu_profile.run(history.fetch_history())
    title = split(r"\s{2}", video)[1]
    video_id = split(r"\s.", video)[0]
    link = "https://www.youtube.com/watch?v=" + video_id
    return (title, video_id, link)


def check_deps():
    deps = ['yt-dlp', 'mpv', 'xclip']
    out = False
    for dep in deps:
        if not which(dep):
            logging.info(f"'{dep}' is not installed")
            out = True
    if out:
        logging.info('install the dependencies..')
        sys.exit(1)


if __name__ == "__main__":
    check_deps()
    logging.info(f"clipboard: {clipboard}")
    choice = dmenu_opts.run(main_opts)

    match choice:
        case "clipboard":
            url = clipboard
            logging.info(url)
        case "play-from-history":
            video = dmenu_his.run(history.fetch_history())
            if video:
                url = "https://www.youtube.com/watch?v=" + split(r"\s.", video)[0]
                logging.info(url)
        case "edit-from-history":
            video = dmenu_his.run(history.fetch_history())
            if video:
                id = split(r"\s.", video)[0]
                history.delete_from_history(id)
                logging.info(f"DELETED: {video}")
                sys.exit(0)
        case "get-description":
            choice = dmenu_desc.run(["clipboard", "history"])
            video_id = split(r"\s{2}", choice)[0]
            match choice:
                case "clipboard":
                    url = clipboard
                case "history":
                    _, _, url = query_video_from_history(dmenu_his, video_id)
                case _:
                    logging.info("exiting...")
                    sys.exit(1)
            logging.info("querying description...\n")
            get_video_description("yt-dlp", "--get-description", url)
            sys.exit(0)
        case _:
            logging.info("exiting...")
            sys.exit(1)

    if not url:
        logging.info("exiting...")
        sys.exit(1)

    data = Data(url).data()
    video_id = data["id"]
    title = data["title"]

    if video_id:
        logging.info(f"TITLE: {title}")
        logging.info(f"VIDEO URL: https://www.youtube.com/watch?v={video_id}\n")

        t1 = threading.Thread(target=play, args=[url])
        t2 = threading.Thread(
            target=get_video_description, args=["yt-dlp", "--get-description", url]
        )

        t1.start()
        t2.start()
        t1.join()
        t2.join()

        history.add(video_id, title)
        input('[Press a key to quit]')

    else:
        logging.error(f"invalid youtube video url: {url}")
        sys.exit(1)

